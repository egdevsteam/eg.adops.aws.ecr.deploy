FROM atlassian/pipelines-awscli:1.16.18

WORKDIR /src
ADD pipe.sh .
ENTRYPOINT [ "/src/pipe.sh" ]
